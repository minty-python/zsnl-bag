#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

mkdir -p /tmp/tests && pytest --junitxml=/tmp/tests/junit.xml --cov=zsnl_bag --cov-report=term
