from unittest import mock

from zsnl_bag.domain.bag.queries import Queries


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestDomainQueries:
    @mock.patch(
        "zsnl_bag.domain.bag.repositories.bag_repository.BagRepository"
    )
    def test_get_bag_record_by_address(self, m_address):
        m_address.get_bag_record_by_address.return_value = (
            "get_bag_record_by_address"
        )
        repository_factory = MockRepositoryFactory("infra_fact")
        repository_factory.repositories["zsnl_bag"] = m_address
        mock_repo = repository_factory.get_repository(
            context=None, name="zsnl_bag"
        )
        q = Queries(
            repository_factory=repository_factory, context=None, user_uuid=None
        )
        call_dict = {
            "postalcode": "1234 AB",
            "housenumber": "6",
            "housenumber_suffix": "11",
            "house_letter": "",
        }
        a = q.get_bag_record_by_address(**call_dict)

        assert mock_repo is m_address
        assert a == "get_bag_record_by_address"
        m_address.get_bag_record_by_address.assert_called_once()

    @mock.patch(
        "zsnl_bag.domain.bag.repositories.bag_repository.BagRepository"
    )
    def test_get_bag_record_by_id(self, m_get_record):

        m_get_record.get_bag_record_by_id.return_value = "get_bag_record_by_id"
        repository_factory = MockRepositoryFactory("infra_fact")
        repository_factory.repositories["zsnl_bag"] = m_get_record
        mock_repo = repository_factory.get_repository(
            context=None, name="zsnl_bag"
        )

        q = Queries(
            repository_factory=repository_factory, context=None, user_uuid=None
        )
        call_dict = {
            "bag_id": "123_bag_id",
            "bag_object_type": "nummeraanduiding",
        }

        a = q.get_bag_record_by_id(**call_dict)

        assert mock_repo is m_get_record
        assert a == "get_bag_record_by_id"
        m_get_record.get_bag_record_by_id.assert_called_once()

    @mock.patch(
        "zsnl_bag.domain.bag.repositories.bag_repository.BagRepository"
    )
    def test_find_nearest_bag_record(self, mock_nearest_bag):

        mock_nearest_bag.find_nearest_bag_record.return_value = (
            "find_nearest_bag_record"
        )
        repository_factory = MockRepositoryFactory("infra_fact")
        repository_factory.repositories["zsnl_bag"] = mock_nearest_bag
        mock_repo = repository_factory.get_repository(
            context=None, name="zsnl_bag"
        )
        q = Queries(
            repository_factory=repository_factory, context=None, user_uuid=None
        )
        call_dict = {"latitude": 12313.12313, "longitude": 123124.123123}
        a = q.find_nearest_bag_record(**call_dict)

        assert mock_repo is mock_nearest_bag
        assert a == "find_nearest_bag_record"
        mock_nearest_bag.find_nearest_bag_record.assert_called_once()

    @mock.patch(
        "zsnl_bag.domain.bag.repositories.bag_repository.BagRepository"
    )
    def test_find_bag_records(self, find_bag_function):

        find_bag_function.find_bag_records.return_value = "find_bag_records"
        repository_factory = MockRepositoryFactory("infra_fact")
        repository_factory.repositories["zsnl_bag"] = find_bag_function
        mock_repo = repository_factory.get_repository(
            context=None, name="zsnl_bag"
        )
        q = Queries(
            repository_factory=repository_factory, context=None, user_uuid=None
        )

        a = q.find_bag_records(
            search_string="hoofdstraat 2",
            priority_municipalities=["Utrecht", "Amsterdam"],
            priority_only=False,
            bag_object_type="nummeraanduiding",
        )

        assert mock_repo is find_bag_function
        assert a == "find_bag_records"
        find_bag_function.find_bag_records.assert_called_once_with(
            search_string="hoofdstraat 2",
            priority_municipalities=["Utrecht", "Amsterdam"],
            priority_only=False,
            bag_object_type="nummeraanduiding",
        )
