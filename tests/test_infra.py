from elasticsearch import Elasticsearch

from zsnl_bag.infra import elasticsearch_from_config


class TestElasticSearchInfra:
    """Test the elasticsearch-from-config infrastructure function."""

    def test_elasticsearch_from_config(self):
        config = {"elasticsearch_bag": {"host": "localhost", "port": 31337}}

        s = elasticsearch_from_config(config=config)

        assert isinstance(s, Elasticsearch)
