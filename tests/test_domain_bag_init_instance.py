from zsnl_bag.domain.bag import get_query_instance
from zsnl_bag.domain.bag.queries import Queries


class TestInitBag:
    def test_get_query_instance(self):
        q = get_query_instance(
            repository_factory={}, context={}, user_uuid=None
        )
        assert isinstance(q, Queries)
