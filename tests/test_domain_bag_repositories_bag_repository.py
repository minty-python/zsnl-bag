import logging
from unittest import mock

from elasticsearch.exceptions import NotFoundError

from zsnl_bag.domain.bag.entities.value_objects import (
    BagID,
    GeoLocation,
    UniqueAddress,
)
from zsnl_bag.domain.bag.repositories import bag_repository


class MockInfraFactoryMock:
    def __init__(self, infra):
        self.infra = infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]


class TestBagRepository:
    @mock.patch("elasticsearch.Elasticsearch")
    def test_get_bag_record_by_id_OK(self, es_client):
        es_client.get.return_value = {"_source": "ES CLIENT CALLED"}
        bag_repo = bag_repository.BagRepository(
            context=None,
            infrastructure_factory=MockInfraFactoryMock(
                {"bag_elasticsearch": es_client}
            ),
            event_service=None,
        )
        bag_id = BagID(id="123_bag_id", bag_object_type="nummeraanduiding")
        res = bag_repo.get_bag_record_by_id(bag_id=bag_id)
        assert res == "ES CLIENT CALLED"

    @mock.patch("elasticsearch.Elasticsearch")
    def test_get_bag_record_by_id_not_found(self, es_client, caplog):
        es_client.get.side_effect = NotFoundError
        caplog.set_level(logging.INFO)
        bag_repo = bag_repository.BagRepository(
            context=None,
            infrastructure_factory=MockInfraFactoryMock(
                {"bag_elasticsearch": es_client}
            ),
            event_service=None,
        )
        caplog.clear()
        bag_id = BagID(id="123_bag_id", bag_object_type="nummeraanduiding")
        res = bag_repo.get_bag_record_by_id(bag_id=bag_id)
        assert res is None
        msg = "BAG record '{}' not found on index: '{}'".format(
            "123_bag_id", "nummeraanduiding"
        )
        assert msg == caplog.records[0].message

    @mock.patch("elasticsearch.Elasticsearch")
    def test_get_bag_record_by_address_OK(self, es_client):

        es_client.search.return_value = {
            "hits": {"hits": [{"_source": "ES_SEARCH FUNCTION CALLED"}]}
        }
        bag_repo = bag_repository.BagRepository(
            context=None,
            infrastructure_factory=MockInfraFactoryMock(
                {"bag_elasticsearch": es_client}
            ),
            event_service=None,
        )
        address = UniqueAddress(
            postalcode="1234 EA",
            housenumber="12",
            house_letter="rood",
            housenumber_suffix="hs",
        )

        res = bag_repo.get_bag_record_by_address(address=address)
        assert res == ["ES_SEARCH FUNCTION CALLED"]

    @mock.patch("elasticsearch.Elasticsearch")
    def test_get_bag_record_by_address_EMPTY_RESULT(self, es_client):

        es_client.search.return_value = {"hits": {"hits": []}}
        bag_repo = bag_repository.BagRepository(
            context=None,
            infrastructure_factory=MockInfraFactoryMock(
                {"bag_elasticsearch": es_client}
            ),
            event_service=None,
        )
        address = UniqueAddress(
            postalcode="1234 EA",
            housenumber="12",
            house_letter="rood",
            housenumber_suffix="hs",
        )

        res = bag_repo.get_bag_record_by_address(address=address)
        assert res == []

    @mock.patch("elasticsearch.Elasticsearch")
    def test_find_nearest_bag_record_OK(self, es_client):

        es_client.search.return_value = {
            "hits": {"hits": [{"_source": "ES_SEARCH FUNCTION CALLED"}]}
        }
        bag_repo = bag_repository.BagRepository(
            context=None,
            infrastructure_factory=MockInfraFactoryMock(
                {"bag_elasticsearch": es_client}
            ),
            event_service=None,
        )
        geo = GeoLocation(latitude=4.23123233, longitude=50.17312231)
        res = bag_repo.find_nearest_bag_record(geolocation=geo)
        assert res == ["ES_SEARCH FUNCTION CALLED"]

    @mock.patch("elasticsearch.Elasticsearch")
    def test_find_nearest_bag_record_EMPTY_RESULT(self, es_client):

        es_client.search.return_value = {"hits": {"hits": []}}
        bag_repo = bag_repository.BagRepository(
            context=None,
            infrastructure_factory=MockInfraFactoryMock(
                {"bag_elasticsearch": es_client}
            ),
            event_service=None,
        )
        geo = GeoLocation(latitude=4.23123233, longitude=50.17312231)
        res = bag_repo.find_nearest_bag_record(geolocation=geo)
        assert res == []

    @mock.patch("elasticsearch.Elasticsearch")
    def test_find_bag_records_OK(self, es_client):

        es_client.search.return_value = {
            "hits": {"hits": [{"_source": "ES_SEARCH FUNCTION CALLED"}]}
        }

        bag_repo = bag_repository.BagRepository(
            context=None,
            infrastructure_factory=MockInfraFactoryMock(
                {"bag_elasticsearch": es_client}
            ),
            event_service=None,
        )
        res = bag_repo.find_bag_records(
            search_string="Kerkstraat 1",
            priority_municipalities=["Amsterdam"],
            priority_only=True,
            bag_object_type=["nummeraanduiding"],
        )
        assert res == ["ES_SEARCH FUNCTION CALLED"]

    def test_find_bag_records_EMPTY_BAG_OBJECT_TYPE(self):
        bag_repo = bag_repository.BagRepository(
            context=None,
            infrastructure_factory=MockInfraFactoryMock(
                {"bag_elasticsearch": {}}
            ),
            event_service=None,
        )
        res = bag_repo.find_bag_records(
            search_string="Kerkstraat 1",
            priority_municipalities=["Amsterdam"],
            priority_only=True,
            bag_object_type=[],
        )
        assert res == []


class TestElasticSearchQueries:
    def test_prepare_es_query_search_options(self):
        from zsnl_bag.domain.bag.repositories import bag_repository

        expected = {
            "sort": [
                {"_score": "desc"},
                {"plaats": "asc"},
                {"huisnummer": "asc"},
                {"huisletter": "asc"},
                {"huisnummer_toevoeging": "asc"},
            ]
        }

        query = bag_repository.prepare_es_query_search(
            search_string="Bergen",
            priority_municipalities=["Bergen op Zoom", "Utrecht", "Amsterdam"],
            priority_only=False,
        )
        assert query["sort"] == expected["sort"]
        assert query["size"] == 10
        assert query["explain"] is False

    def test_prepare_es_query_search_priority_only(self):
        from zsnl_bag.domain.bag.repositories import bag_repository

        query = bag_repository.prepare_es_query_search(
            search_string="Bergen",
            priority_municipalities=["Utrecht", "Amsterdam"],
            priority_only=True,
        )
        expected = {
            "bool": {
                "should": [
                    {"term": {"gemeente": {"value": "Utrecht", "boost": 1.9}}},
                    {
                        "term": {
                            "gemeente": {"value": "Amsterdam", "boost": 1.9}
                        }
                    },
                ]
            }
        }

        assert query["query"]["bool"]["filter"] == expected

    def test_prepare_es_query_search_postalcode(self):
        from zsnl_bag.domain.bag.repositories import bag_repository

        expected = [
            {
                "match": {
                    "autocomplete_index": {"query": "Bergen 1023 hb straat"}
                }
            },
            {
                "match": {
                    "_id": {"query": "Bergen 1023 hb straat", "boost": 100}
                }
            },
            {
                "match": {
                    "huisnummer_autocomplete": {
                        "query": "Bergen 1023 hb straat",
                        "boost": 0.6,
                    }
                }
            },
            {"match": {"postcode": {"query": "1023HB", "boost": 1.2}}},
        ]

        query = bag_repository.prepare_es_query_search(
            search_string="Bergen 1023 hb straat",
            priority_municipalities="",
            priority_only=False,
        )
        assert query["query"]["bool"]["must"]["bool"]["should"] == expected

    def test_prepare_es_query_nearest_records(self):
        from zsnl_bag.domain.bag.repositories import bag_repository

        expected = {
            "size": 1,
            "query": {
                "bool": {
                    "must": {
                        "function_score": {
                            "gauss": {
                                "geo_lat_lon": {
                                    "origin": {
                                        "lat": 1.12345,
                                        "lon": 50.98765,
                                    },
                                    "offset": "1m",
                                    "scale": "50m",
                                }
                            }
                        }
                    },
                    "filter": {
                        "geo_distance": {
                            "distance": "500m",
                            "geo_lat_lon": {"lat": 1.12345, "lon": 50.98765},
                        }
                    },
                }
            },
        }
        query = bag_repository.prepare_es_query_nearest_records(
            latitude=1.12345, longitude=50.98765
        )
        assert query == expected

    def test_extract_postalcode(self):
        ok = {
            "1234 ab": "1234AB",
            "1234 AB": "1234AB",
            "1234ab": "1234AB",
            "1234AB": "1234AB",
            "12 2651DN 21": "2651DN",
            "6789ba 1234Ab": "6789BA",
            "street 2234 ba 5": "2234BA",
            "street 1234 ba": "1234BA",
            "1944 ab b": "1944AB",
        }

        fail = [
            "1234ABb",
            "1234 ABS",
            "straat 2312a-2 ",
            "plein 1944 4",
            "plein 1944 abm",
            "122651 DN 21",
        ]
        for string, expected in ok.items():
            answer = bag_repository.extract_postalcode(search_string=string)
            assert answer == expected

        for string in fail:
            answer = bag_repository.extract_postalcode(search_string=string)
            assert answer is None
