from elasticsearch import Elasticsearch


def elasticsearch_from_config(config: dict) -> Elasticsearch:
    """Get initialized ElasticSearch client from config.

    :param config: should contain:
        `{'elasticsearch_bag:{
                {
                'host': 'elasticsearch_host',
                'port': 1234
                }
            }}`
    :type config: dict
    :return: initialized elasticsearch client
    :rtype: Elasticsearch
    """
    es_config = config["elasticsearch_bag"]

    client = Elasticsearch(**es_config)
    return client
