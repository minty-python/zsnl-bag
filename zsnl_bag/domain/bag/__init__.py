from .queries import Queries
from .repositories.bag_repository import BagRepository

REQUIRED_REPOSITORIES = {"zsnl_bag": BagRepository}


def get_query_instance(repository_factory, context, user_uuid):
    return Queries(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )
