from typing import NamedTuple


class GeoLocation(NamedTuple):
    """Geographical location consisting of latitude and longitude."""

    latitude: float
    longitude: float


class UniqueAddress(NamedTuple):
    """Characteristics that uniquely identify an address in the Netherlands."""

    postalcode: str
    housenumber: int
    housenumber_suffix: str
    house_letter: str


class BagID(NamedTuple):
    """Uniquely identifies BAG records."""

    bag_object_type: str
    id: str
