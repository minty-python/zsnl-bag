import re

from elasticsearch.exceptions import NotFoundError
from minty.repository import RepositoryBase

from ....infra import elasticsearch_from_config
from ..entities.value_objects import BagID, GeoLocation, UniqueAddress


class BagRepository(RepositoryBase):
    """BAG Repository."""

    __slots__ = ["infrastructure_factory", "context"]

    REQUIRED_INFRASTRUCTURE = {"bag_elasticsearch": elasticsearch_from_config}
    TYPES = {
        "nummeraanduiding": "bag_nummeraanduiding",
        "openbareruimte": "bag_openbareruimte",
    }

    def _search(self, index_types: list, es_query: dict) -> list:

        self.statsd.get_counter("bag_search").increment("count")

        types = [self.TYPES[index] for index in index_types]
        if not types:
            return []

        es = self.infrastructure_factory.get_infrastructure(
            context=self.context, infrastructure_name="bag_elasticsearch"
        )
        with self.statsd.get_timer("bag_search").time("time"):
            result = es.search(
                index=types,
                doc_type="_doc",
                body=es_query,
                search_type="dfs_query_then_fetch",
            )
        result = [res["_source"] for res in result["hits"]["hits"]]
        return result

    def get_bag_record_by_id(self, bag_id: BagID) -> dict:
        """Get single BAG record by BAG id and bag_object_type.

        :param bag_id: BagID value object
        :type bag_id: BagID
        :return: result of ElasticSearch GET request
        :rtype: dict
        """
        es = self.infrastructure_factory.get_infrastructure(
            context=self.context, infrastructure_name="bag_elasticsearch"
        )
        try:
            with self.statsd.get_timer("bag_get").time("time"):
                result = es.get(
                    id=bag_id.id,
                    index=self.TYPES[bag_id.bag_object_type],
                    doc_type="_doc",
                )

            return result["_source"]
        except NotFoundError:
            self.logger.info(
                f"BAG record '{bag_id.id}' not found on index: '{bag_id.bag_object_type}'"
            )
            return None

    def get_bag_record_by_address(self, address: UniqueAddress) -> list:
        """Get single BAG record for given address and BAG index type.

        :param address: address value object
        :param type address: UniqueAddress
        :return: found record(s)
        :rtype: list
        """
        bag_object_type = ["nummeraanduiding"]

        es_query = {
            "query": {
                "bool": {
                    "filter": [
                        {"term": {"postcode": address.postalcode}},
                        {"term": {"huisnummer": address.housenumber}},
                        {"term": {"huisletter": address.house_letter}},
                        {
                            "term": {
                                "huisnummer_toevoeging": address.housenumber_suffix
                            }
                        },
                    ],
                    "should": {
                        "bool": {
                            "must_not": {
                                "term": {"status": "Naamgeving ingetrokken"}
                            }
                        }
                    },
                }
            }
        }
        result = self._search(index_types=bag_object_type, es_query=es_query)
        return result

    def find_nearest_bag_record(self, geolocation: GeoLocation) -> list:
        """Find BAG records geographically nearest to given location.

        :param geolocation: geolcation value object
        :type geolocation: GeoLocation
        :return:  found record(s)
        :rtype: list
        """
        bag_object_type = ["nummeraanduiding"]
        es_query = prepare_es_query_nearest_records(
            latitude=geolocation.latitude, longitude=geolocation.longitude
        )
        result = self._search(index_types=bag_object_type, es_query=es_query)
        return result

    def find_bag_records(
        self,
        search_string: str,
        priority_only: bool,
        priority_municipalities: list,
        bag_object_type: str,
    ) -> dict:
        """Find BAG records for given search string.

        :param search_string: string to search for
        :type search_string: str
        :param priority_only: Only search in priority municipalities
        :type priority_only: bool
        :param priority_municipalities: priority municipalities
        :type priority_municipalities: list
        :param bag_object_type: elasticsearch indeces to search in
        :param bag_object_type: list
        :return:  found record(s)
        :rtype: dict
        """
        es_query = prepare_es_query_search(
            search_string=search_string,
            priority_municipalities=priority_municipalities,
            priority_only=priority_only,
        )

        return self._search(index_types=bag_object_type, es_query=es_query)


def prepare_es_query_search(
    search_string: str, priority_municipalities: list, priority_only: bool
) -> dict:
    """Prepare ElasticSearch query to find for BAG records.

    :param search_string: String to search for
    :type search_string: str
    :param priority_municipalities: list of municipalities to give priority to
    :type priority_municipalities: list
    :param priority_only: restrict search only to list of municipalities
    :type priority_only: bool
    :return: ElasticSearch formatted query
    :rtype: dict
    """

    match_constraints = [
        {"match": {"autocomplete_index": {"query": search_string}}},
        {"match": {"_id": {"query": search_string, "boost": 100}}},
        {
            "match": {
                "huisnummer_autocomplete": {
                    "query": search_string,
                    "boost": 0.6,
                }
            }
        },
    ]

    postalcode = extract_postalcode(search_string)
    if postalcode:
        match_constraints.append(
            {"match": {"postcode": {"query": postalcode, "boost": 1.2}}}
        )

    es_query = {}
    es_query["bool"] = {"must": {"bool": {"should": match_constraints}}}
    priority_list = []
    for mun in priority_municipalities:
        priority_list.append(
            {"term": {"gemeente": {"value": mun, "boost": 1.9}}}
        )
    if priority_list:
        if priority_only:
            es_query["bool"]["filter"] = {"bool": {"should": priority_list}}
        else:
            es_query["bool"]["should"] = priority_list

    es_query["bool"]["must_not"] = {
        "term": {"status": "Naamgeving ingetrokken"}
    }

    sorting = [
        {"_score": "desc"},
        {"plaats": "asc"},
        {"huisnummer": "asc"},
        {"huisletter": "asc"},
        {"huisnummer_toevoeging": "asc"},
    ]
    query = {"explain": False, "size": 10, "query": es_query, "sort": sorting}
    return query


def extract_postalcode(search_string: str):
    """extract_postalcode from `search_string`.

    :param search_string: search string
    :type search_string: str
    :return: Postalcode in format `1234AB`
    :rtype: str or None
    """
    regex = r"\b[0-9]{4}[ ]?[a-z]{2}\b"
    r = re.search(pattern=regex, string=search_string, flags=re.I)
    if r:
        r = r[0].replace(" ", "").upper()
    return r


def prepare_es_query_nearest_records(
    latitude: float, longitude: float
) -> dict:
    """Prepare ElasticSearch Query to find nearest record.

    :param latitude: latitude
    :type latitude: float
    :param longitude: longitude
    :type longitude: float
    :return: elasticearch query
    :rtype: dict
    """
    NEAREST_OFFSET = "1m"
    NEAREST_SCALE = "50m"
    NEAREST_MAX_DISTANCE = "500m"

    es_query = {"size": 1, "query": {"bool": {"must": ""}}}
    es_query["query"]["bool"]["must"] = {
        "function_score": {
            "gauss": {
                "geo_lat_lon": {
                    "origin": {"lat": latitude, "lon": longitude},
                    "offset": NEAREST_OFFSET,
                    "scale": NEAREST_SCALE,
                }
            }
        }
    }
    es_query["query"]["bool"]["filter"] = {
        "geo_distance": {
            "distance": NEAREST_MAX_DISTANCE,
            "geo_lat_lon": {"lat": latitude, "lon": longitude},
        }
    }
    return es_query
