from minty.cqrs import QueryBase

from .entities.value_objects import BagID, GeoLocation, UniqueAddress


class Queries(QueryBase):
    def get_bag_record_by_id(self, bag_id: str, bag_object_type: str):
        """Get single BAG record by BAG id and BAG object type.

        :param bag_id: BAG identifier
        :type bag_id: str
        :param bag_object_type: BAG object type
        :type bag_object_type: str
        :return: search results
        :rtype: dict
        """
        bag_id = BagID(id=bag_id, bag_object_type=bag_object_type)
        repo = self.repository_factory.get_repository(
            context=None, name="zsnl_bag"
        )
        return repo.get_bag_record_by_id(bag_id=bag_id)

    def get_bag_record_by_address(
        self,
        postalcode: str,
        housenumber: int,
        house_letter: str,
        housenumber_suffix: str,
    ):
        """Get single BAG record for given address and BAG object type.

        :param postalcode: postalcode
        :type postalcode: str
        :param housenumber: housenumber
        :type housenumber: int
        :param house_letter: house letter
        :type house_letter: str
        :param housenumber_suffix: housenumber supplement
        :type housenumber_suffix: str
        :return: search results
        :rtype: list
        """
        address = UniqueAddress(
            postalcode=postalcode,
            housenumber=housenumber,
            house_letter=house_letter,
            housenumber_suffix=housenumber_suffix,
        )
        repo = self.repository_factory.get_repository(
            context=None, name="zsnl_bag"
        )
        return repo.get_bag_record_by_address(address=address)

    def find_nearest_bag_record(self, latitude: float, longitude: float):
        """Find BAG record geographically nearest to given latitude and longitude.

        :param latitude: latitude
        :type latitude: float
        :param longitude: longitude
        :type longitude: float
        :return: search results
        :rtype: list
        """
        geolocation = GeoLocation(latitude=latitude, longitude=longitude)

        repo = self.repository_factory.get_repository(
            context=None, name="zsnl_bag"
        )
        return repo.find_nearest_bag_record(geolocation=geolocation)

    def find_bag_records(
        self,
        search_string: str,
        priority_municipalities: list,
        priority_only: bool,
        bag_object_type: str,
    ):
        """Find BAG records for given search string.

        `priority_municipalities` param will show results from the given
        municipalities first. `priority_only` flag will exclusively show records
        from the priority municipality list.

        :param search_string: string to search for
        :type search_string: str
        :param priority_municipalities: priority municipalities
        :type priority_municipalities: list
        :param priority_only: priority municipalities only flag
        :type priority_only: bool
        :param bag_object_type: bag object type
        :type bag_object_type: str
        :return: search results
        :rtype: list
        """
        repo = self.repository_factory.get_repository(
            context=None, name="zsnl_bag"
        )
        return repo.find_bag_records(
            search_string=search_string,
            priority_municipalities=priority_municipalities,
            priority_only=priority_only,
            bag_object_type=bag_object_type,
        )
