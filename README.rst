.. _readme:

Introduction
============

Zaaksysteem domain implementation for BAG services

Getting started
---------------

See the Minty base repository for more information:
`<https://gitlab.com/minty-python/minty/blob/master/README.rst>`_

More documentation
------------------

Please see the generated documentation via CI for more information about this
module and how to contribute in our online documentation. Open index.html
when you get there:
`<https://gitlab.com/minty-python/zsnl-bag/-/jobs/artifacts/master/browse/tmp/docs?job=qa>`_


Contributing
------------

Please read `CONTRIBUTING.md <https://gitlab.com/minty-python/zsnl-bag/blob/master/CONTRIBUTING.rst>`_
for details on our code of conduct, and the process for submitting pull requests to us.

Versioning
----------

We use `SemVer <https://semver.org/>`_ for versioning. For the versions
available, see the
`tags on this repository <https://gitlab.com/minty-python/zsnl-bag/tags/>`_

License
-------

Copyright (c) 2018, Minty Team and all persons listed in
`CONTRIBUTORS <https://gitlab.com/minty-python/zsnl-bag/blob/master/CONTRIBUTORS.rst>`_

This project is licensed under the EUPL, v1.2. See the
`EUPL-1.2.txt <https://gitlab.com/minty-python/zsnl-bag/blob/master/LICENSE>`_
file for details.
